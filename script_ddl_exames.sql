create table laboratorio (
        id int8 not null,
        ativo boolean not null,
        celular varchar(15),
        cpfCnpj varchar(14),
        email varchar(100),
        nome varchar(80) not null,
        telefone varchar(15),
        primary key (id)
    )

create table categoria_exame (
        id int8 not null,
        ativo boolean not null,
        nome varchar(50) not null,
        laboratorio_id int8 not null,
        primary key (id)
    )

    create table tipo_exame (
        id int8 not null,
        ativo boolean not null,
        nome varchar(100) not null,
        categoria_exame_id int8 not null,
        primary key (id)
    )
    
    create table solicitacao_exame (
        id int8 not null,
        codigo_exame varchar(10),
        data_hora_atualizacao timestamp,
        data_hora_coleta timestamp,
        data_hora_conclusao timestamp,
        data_hora_criacao timestamp,
        data_hora_processamento timestamp,
        data_hora_recebimento timestamp,
        responsavel varchar(50),
        status varchar(20) not null,
        animal_id int8 not null,
        laboratorio_id int8,
        veterinario_id int8,
        usuario_atualizou_id int8,
        usuario_concluiu_id int8,
        usuario_criou_id int8,
        usuario_recebeu_id int8,
        primary key (id)
    )

    create table solicitacao_exame_item (
        id int8 not null,
        outroTipoExame varchar(50),
        categoria_exame_id int8 not null,
        solicitacao_exame_id int8 not null,
        tipo_exame_id int8 not null,
        primary key (id)
    )

    alter table laboratorio 
        drop constraint UK_laa5lb1gkd6y08aio0lcctmax


    alter table laboratorio 
        add constraint UK_laa5lb1gkd6y08aio0lcctmax unique (cpfCnpj)

create sequence laboratorio_id_seq start 2 increment 1
create sequence categoria_exame_id_seq start 10 increment 1
create sequence tipo_exame_id_seq start 67 increment 1
create sequence solicitacao_exame_id_seq start 1 increment 1
create sequence solicitacao_exame_item_id_seq start 1 increment 1

    alter table categoria_exame 
        add constraint fk_categoria_exame__laboratorio_id 
        foreign key (laboratorio_id) 
        references laboratorio

    alter table tipo_exame 
        add constraint fk_tipo_exame__categoria_exame_id 
        foreign key (categoria_exame_id) 
        references categoria_exame
        
    alter table solicitacao_exame 
        add constraint fk_solicitacao_exame__animal_id 
        foreign key (animal_id) 
        references animal

    alter table solicitacao_exame 
        add constraint fk_solicitacao_exame__laboratorio_id 
        foreign key (laboratorio_id) 
        references laboratorio

    alter table solicitacao_exame 
        add constraint fk_solicitacao_exame__veterinario_id 
        foreign key (veterinario_id) 
        references veterinario


    alter table solicitacao_exame 
        add constraint fk_solicitacao_exame__usuario_atualizou_id 
        foreign key (usuario_atualizou_id) 
        references usuario


    alter table solicitacao_exame 
        add constraint fk_solicitacao_exame__usuario_concluiu_id 
        foreign key (usuario_concluiu_id) 
        references usuario

    alter table solicitacao_exame 
        add constraint fk_solicitacao_exame__usuario_criou_id 
        foreign key (usuario_criou_id) 
        references usuario

    alter table solicitacao_exame 
        add constraint fk_solicitacao_exame__usuario_recebeu_id 
        foreign key (usuario_recebeu_id) 
        references usuario


    alter table solicitacao_exame_item 
        add constraint fk_solicitacao_exame_item__categoria_exame_id 
        foreign key (categoria_exame_id) 
        references categoria_exame


    alter table solicitacao_exame_item 
        add constraint fk_solicitacao_exame_item__solicitacao_exame_id 
        foreign key (solicitacao_exame_id) 
        references solicitacao_exame


    alter table solicitacao_exame_item 
        add constraint fk_solicitacao_exame_item__tipo_exame_id 
        foreign key (tipo_exame_id) 
        references tipo_exame
