

-------- 09/05/17 17:00

alter table atendimento_paciente add column laudo_ultrassom text;

update configuracao set chave = 'ATENDIMENTO_LAUDO_ULTRASSOM' where id = 4;

-------- 05/05/17 13:00 (script rodado adiantado, nas do dia 09/05)

insert into configuracao (id, chave, valor) values
(4, 'ATENDIMENTO_LAUDO_ULTRASSOM',
'<div style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal; text-align: center;"><span style="font-weight: bold;">UNIVERSIDADE ESTADUAL DO CEARÁ - UECE</span></div><div style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal; text-align: center;"><span style="font-weight: bold;">FACULDADE DE VETERINÁRIA - FAVET</span></div><div style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal; text-align: center;"><span style="font-weight: bold;">HOSPITAL VETERINÁRIO - HVSBC</span></div><div style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal; text-align: center;"><br></div><div style="font-family: Arial, Verdana; font-size: 10pt;"><span style="font-size: 10pt; font-weight: bold;">Animal:</span><span style="font-size: 10pt;">&nbsp;&nbsp;</span></div><div style="font-family: Arial, Verdana; font-size: 10pt;"><span style="font-weight: bold;">Raça:</span><span style="font-weight: normal;">&nbsp;</span></div><div style="font-family: Arial, Verdana; font-size: 10pt;"><span style="font-weight: bold;">Proprietário:</span><span style="font-weight: normal;">&nbsp;</span></div><div style="font-family: Arial, Verdana; font-size: 10pt;"><br></div><div style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Fígado:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
tamanho preservado, com contorno regular, ecogenicidade padrão, sem
indícios de cistos ou massas. Vasos hepáticos e portais de calibre
e trajeto normais. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></div><div style="font-weight: normal;">
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-top: 0.42cm; margin-bottom: 0cm; line-height: 100%;">
<font face="Arial, serif"><font style="font-size: 10pt"><b>Vesícula
biliar:</b></font></font><font face="Arial, serif"><font style="font-size: 10pt">
paredes finas, apresentando ecogenicidade padrão e com conteúdo
anecogênico. </font></font><font face="Arial, serif"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Baço:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
tamanho padrão, apresentando bordas regulares, contorno liso e
regular, com ecogenicidade normal, sem indícios de massas ou cistos.
</font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Estômago:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
preenchido por conteúdo mucóide, apresentando paredes normoespessas
(cm) e com estratificação parietal preservada. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Alças
intestinais:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
com paredes preservadas (cm) nos segmentos avaliados, repletas por
conteúdo gasoso/mucóide. Presença de peristaltismo evolutivo e
uniforme. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Pâncreas:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
visualizado em região de ramo direito, apresentando textura e
ecogenicidade habituais e dimensões normais (cm de espessura).
</font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Rins:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
de tamanho normal, medindo respectivamente: RE: cm e RD: cm, em
topografia habitual, com contornos regulares, arquitetura preservada.
Cortical de espessura e ecogenicidade normais. Relação
córtico-medulares preservada. Ausência de cistos, massas, cálculos
ou hidronefrose. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Glândulas
adrenais:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
com ecotextura homogênea, contornos regulares e dimensões normais
(AE: cm de comprimento X cm de altura em pólo caudal e AD: cm de
comprimento X cm de altura em pólo caudal). </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Bexiga
urinária:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
em adequada repleção, com paredes finas e regulares. Preenchida por
conteúdo anecogênico. Sem evidências de litíase. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Próstata:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
de dimensões normais, medindo cerca de cm de comprimento por cm de
altura por cm de largura. Apresenta contornos regulares e ecotextura
preservada. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Testículos:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
em topografia normal, contornos regulares, ecotextura e linhas
hiperecogênicas mediastinais preservadas. Mediram em maior eixo
cerca de TD = cm e TE = cm. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Corpo
e cornos uterinos:</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">
de dimensões preservadas, medindo aproximadamente cm de diâmetro,
apresentando paredes regulares e sem evidências de conteúdo
luminal. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Ovários:
</b></font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt">em
topografia normal, com contornos regulares, formato ovalado e
ecotextura preservada. Mediram cerca de cm de comprimento à direita
e cm de comprimento à esquerda. </font></font><font face="Arial, serif" style="font-size: 12pt;"><font style="font-size: 10pt"><b>Aspecto
sonográfico padrão.</b></font></font></p>
<p class="western" align="justify" style="margin-bottom: 0cm; line-height: 100%;"><u><b><font face="Arial, serif"><span style="font-size: 10pt;">Observações:</span></font><br></b></u><span style="font-family: Arial, serif; font-size: 10pt;">1-Sem
evidências de linfonodomegalias e líquido livre abdominal.<br></span><span style="font-family: Arial, serif; font-size: 10pt;">2-A
ultrassonografia é uma avaliação complementar, de caráter
pontual, devendo ser interpretada juntamente com os sinais clínicos
e/ou exames laboratoriais, sempre a critério do médico(a)
veterinário(a) que acompanha o animal.</span></p>
<p class="western" style="font-family: Arial, Verdana; font-size: 10pt; margin-bottom: 0cm; line-height: 100%;"><br></p></div><div style="font-family: Arial, Verdana; font-size: 10pt; text-align: right; font-weight: normal;">Fortaleza-CE, &nbsp;/ &nbsp; /</div><div style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal;"><br></div><div style="font-family: Arial, Verdana; font-size: 10pt; text-align: center;"><span style="font-weight: bold;">______________________________________</span></div><div style="font-family: Arial, Verdana; font-size: 10pt; text-align: center;"><span style="font-weight: bold;">&nbsp; Dr(a). &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CRMV:</span></div><style type="text/css">
	<!--
		@page { margin: 2cm }
		p { margin-bottom: 0.25cm; direction: ltr; line-height: 120%; text-align: left; widows: 2; orphans: 2 }
		p.western { font-family: "Times New Roman", serif; font-size: 12pt }
		p.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: pt-BR }
		p.ctl { font-family: "Times New Roman"; font-size: 12pt }
	-->
	</style>');

-------- 05/05/17 12:00  Próxima Versão

ATENÇÃO: atualizar no PgAdmin para "current value" do índice raca_id_seq (para 107 ?)
ATENÇÃO: atualizar no PgAdmin para "current value" do índice especie_id_seq (para 11)
ATENÇÃO: atualizar no PgAdmin para "current value" do índice veterinario_id_seq

insert into consultorio (id, nome) values (9,'Exames - Ultrason');

alter table veterinario add column clinico boolean;
alter table veterinario add column faz_exames boolean;
update veterinario set clinico = true, faz_exames = false;
-- Editar clinico = false para os veterinários cirurgiões e que fazem Exames

-- campo para atendente da cirurgia que poderá alterar o status do atendimento
alter table usuario add column pode_iniciar_atendimento boolean;
update usuario set pode_iniciar_atendimento = false;

-------- 03/05/17  

ATENÇÃO: atualizar no PgAdmin para "current value" do índice usuario_id_seq  

alter table usuario add column login varchar(20);
alter table usuario add column data_hora_alteracao timestamp;
alter table usuario add column redefinir boolean;
alter table usuario add column ativo boolean;
update usuario set redefinir = false;
update usuario set ativo = true;

-------- 02/05/17

CREATE SEQUENCE upload_arquivo_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 11
  CACHE 1;
ALTER TABLE upload_arquivo_id_seq
  OWNER TO postgres;



CREATE TABLE upload_arquivo
(
  id bigint NOT NULL,
  caminho_diretorio character varying(255),
  content_type character varying(30),
  data_hora_exclusao timestamp without time zone,
  data_hora_upload timestamp without time zone,
  modulo_sistema character varying(30) NOT NULL,
  nome_destino character varying(255),
  nome_origem character varying(255),
  atendimento_paciente_id bigint,
  usuario_id bigint,
  descricao character varying(50),
  CONSTRAINT upload_arquivo_pkey PRIMARY KEY (id),
  CONSTRAINT fk_upload_arquivo_atendimento_paciente_atendimento_paciente_id FOREIGN KEY (atendimento_paciente_id)
      REFERENCES atendimento_paciente (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_upload_arquivo_usuario_usuario_id FOREIGN KEY (usuario_id)
      REFERENCES usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE upload_arquivo
  OWNER TO postgres;


-------- 17/04/17 - v 1.3.4
update atendimento_paciente set tipo_atendimento_paciente = 'CIRURGICO' where tipo_atendimento_paciente = 'CIGURGICO'


alter table atendimento_paciente add column exame_complementar2 text;
alter table atendimento_paciente add column resultado_exame_complementar2 text;
alter table atendimento_paciente add column tratamento_cirurgico text;
alter table veterinario add column cirurgiao boolean;

update veterinario set cirurgiao = false;

update atendimento_paciente set
       exame_complementar2 = exame_complementar, 
       resultado_exame_complementar2 = resultado_exame_complementar
where exame_complementar is not null OR resultado_exame_complementar is not null           


---------   v 1.3.3
alter table atendimento_paciente add column peso double precision;


--------- v 1.2.0

alter table atendimento_paciente add column exame_complementar varchar(255);
alter table atendimento_paciente add column resultado_exame_complementar varchar(255);

CREATE TABLE configuracao
(
  id bigint NOT NULL,
  chave character varying(100) NOT NULL,
  valor text NOT NULL,
  CONSTRAINT configuracao_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE configuracao
  OWNER TO postgres;

create sequence configuracao_id_seq start 1 increment 1;

insert into configuracao values (1,'ATENDIMENTO_ANAMNESE',
'Queixa Principal: 

Histórico:

Vacinação:

Vermifugação:

Ectoparasitas:

Histórico de Reprodução:

Alimentação:

Doenças Anteriores:

Ambiente:

Procedimentos Realizados:

Contactante:

Medicação utilizada: ');

insert into configuracao values (2,'ATENDIMENTO_RECEITUARIO','<div style="font-weight: normal; text-align: center;"><span style="font-weight: bold;">UNIVERSIDADE ESTADUAL DO CEARÁ - UECE</span></div><div style="font-weight: normal; text-align: center;"><span style="font-weight: bold;">FACULDADE DE VETERINÁRIA - FAVET</span></div><div style="font-weight: normal; text-align: center;"><span style="font-weight: bold;">HOSPITAL VETERINÁRIO - HVSBC</span></div><div style="font-weight: normal; text-align: center;"><br></div><div><span style="font-weight: bold;"><br></span></div><div><span style="font-weight: bold;">Animal:</span><span style="font-weight: normal;">&nbsp;</span></div><div><span style="font-weight: bold;"><br>Raça:</span><span style="font-weight: normal;">&nbsp;</span></div><div><span style="font-weight: bold;"><br>Proprietário:</span><span style="font-weight: normal;">&nbsp;</span></div><div style="font-weight: normal;"><br></div><div style="font-weight: normal;"><br></div><div><br></div><div><br></div><div style="text-align: right; font-weight: normal;">Fortaleza-CE, DD/MM/AAAA</div><div style="font-weight: normal;"><br></div><div style="text-align: center;"><span style="font-weight: bold;">_____________________________________</span></div><div style="text-align: center;"><span style="font-weight: bold;">&nbsp; Dr(a). &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; CRMV:</span></div>');
insert into configuracao values (3,'ATENDIMENTO_AVALIACAO_FISICA',
'Estado Geral:
Temp.:
Hidratação:
Linfonodos:
FC.:    /min.
Pulso:  /min.
FR:     /min.
Mucosas:

Olhos:
Orelhas:
Ectoparasitas:
Pele:

Cardiovascular:

Respiratório:

Digestório:

Urinário:

Reprodutivo:

Neurológico: ');


--------- v 1.1.0

﻿--cadastro de Veterinários
insert into veterinario (id, nome, ativo) values (1, 'Doutor(a) 1', 't');
insert into veterinario (id, nome, ativo) values (2, 'Doutor(a) 2', 't');
insert into veterinario (id, nome, ativo) values (3, 'Doutor(a) 3', 't');
insert into veterinario (id, nome, ativo) values (4, 'Doutor(a) 4', 't');
insert into veterinario (id, nome, ativo) values (5, 'Doutor(a) 5', 't');
insert into veterinario (id, nome, ativo) values (6, 'Doutor(a) 6', 't');
insert into veterinario (id, nome, ativo) values (7, 'Doutor(a) 7', 'f');
insert into veterinario (id, nome, ativo) values (8, 'Doutor(a) 8', 'f');
--SAVEPOINT veterinarios_salvos;

--cadastro de Grupos de Usuarios
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (1, 'Administradores', 'Grupo dos Administradores do Sistema','ADMINISTRADOR');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (2, 'Assistentes de Atendimento', 'Grupo dos Assistentes de Atendimento','ASSISTENTE_ATENDIMENTO');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (3, 'Assistentes de Farmácia', 'Grupo dos Assistentes de Atendimento','ASSISTENTE_FARMACIA');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (4, 'Veterinários', 'Grupo dos Veterinários','VETERINARIO');
insert into grupo (id, nome, descricao, tipo_grupo_usuario) values (5, 'Gestores', 'Grupo dos Gestores','GESTOR');

--cadastro de Usuarios
insert into usuario (id, nome, email, senha, grupo_id) values (1, 'spaixaox', 'spaixaox@gmail.com', 'b1ba5f5682c76b4af5e97ee5cd1292b7', 1); --Mentor Kardec (limpo) +  nr EB + semicolon;
insert into usuario (id, nome, email, senha, grupo_id) values (2, 'Wanamark', 'manoel.wanamark@uece.br', 'e4a01e028c1f83e0caf7baeaa7d17e65', 1); --Wana2017');
insert into usuario (id, nome, email, senha, grupo_id) values (3, 'Atendimento 1', 'atendimento1@favet.uece.br', 'aa05e45e301a3ba917eb365c76ec71ec', 2); -- 'Atend10';
insert into usuario (id, nome, email, senha, grupo_id) values (4, 'Atendimento 2', 'atendimento2@favet.uece.br', 'aa05e45e301a3ba917eb365c76ec71ec', 2); -- 'Atend10';
insert into usuario (id, nome, email, senha, grupo_id) values (5, 'Atendimento 3', 'atendimento3@favet.uece.br', 'aa05e45e301a3ba917eb365c76ec71ec', 2); -- 'Atend10';
insert into usuario (id, nome, email, senha, grupo_id) values (6, 'Atendimento 4', 'atendimento4@favet.uece.br', 'aa05e45e301a3ba917eb365c76ec71ec', 2); -- 'Atend10';
insert into usuario (id, nome, email, senha, grupo_id) values (7, 'Farmácia 1', 'atendimentof1@favet.uece.br', '357e5c28afae0027d57daa517d6a12cd', 3); -- 'Farmacia10';
insert into usuario (id, nome, email, senha, grupo_id) values (8, 'Farmácia 2', 'atendimentof2@favet.uece.br', '357e5c28afae0027d57daa517d6a12cd', 3); -- 'Farmacia10';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (9, 'Veterinario 1', 'veterinario1@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 1); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (10, 'Veterinario 2', 'veterinario2@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 2); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (11, 'Veterinario 3', 'veterinario3@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 3); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (12, 'Veterinario 4', 'veterinario4@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 4); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (13, 'Veterinario 5', 'veterinario5@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 5); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (14, 'Veterinario 6', 'veterinario6@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 6); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (15, 'Veterinario 7', 'veterinario7@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 7); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id, veterinario_id) values (16, 'Veterinario 8', 'veterinario8@favet.uece.br', 'cd0f61fa9256537cd7b483c8173040f0', 4, 8); --'CRMV2017';
insert into usuario (id, nome, email, senha, grupo_id) values (17, 'Gestor 1', 'gestor1@uece.br', '253680d72cd633980caf85196561fe0d',5);--'Gestao2017';
insert into usuario (id, nome, email, senha, grupo_id) values (18, 'Gestor 2', 'gestor2@uece.br', '253680d72cd633980caf85196561fe0d',5);--'Gestao2017';
insert into usuario (id, nome, email, senha, grupo_id) values (19, 'Gestor 3', 'gestor3@uece.br', '253680d72cd633980caf85196561fe0d',5);--'Gestao2017';
insert into usuario (id, nome, email, senha, grupo_id) values (20, 'Gestor 4', 'gestor4@uece.br', '253680d72cd633980caf85196561fe0d',5);--'Gestao2017';

update usuario set senha = 'Favet'

--Cadastro de Consultórios
insert into consultorio (id, nome) values (1, 'Consultório 1');
insert into consultorio (id, nome) values (2, 'Consultório 2');
insert into consultorio (id, nome) values (3, 'Consultório 3');
insert into consultorio (id, nome) values (4, 'Consultório 4');
insert into consultorio (id, nome) values (5, 'Consultório 5');
insert into consultorio (id, nome) values (6, 'Consultório 6');
insert into consultorio (id, nome) values (7, 'Centro Cirúrgico 1');
insert into consultorio (id, nome) values (8, 'Centro Cirúrgico 2');

--Cadastro de Espécies
insert into especie values(1,'Caninos');
insert into especie values(2,'Felinos');
insert into especie values(3,'Lagomorfos (Coelhos)');
insert into especie values(4,'Roedores');
insert into especie values(5,'Psitacídeos (Ordem de Aves)');
insert into especie values(6,'Bovinos');
insert into especie values(7,'Caprinos');
insert into especie values(8,'Equinos');
insert into especie values(9,'Ovinos');
insert into especie values(10,'Suinos');
insert into especie values(11,'Outros (Sem Cadastro)');
--SAVEPOINT especies_salvas;

--Cadastro de Raças
--Outras Raças para cada espécie já que há dependência na hora do cadastro
insert into raca (id, nome, especie_id, tipo_porte) values (1, 'Outros (Sem Cadastro)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (2, 'Outros (Sem Cadastro)', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (3, 'Outros (Sem Cadastro)', 3, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (4, 'Outros (Sem Cadastro)', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (5, 'Outros (Sem Cadastro)', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (6, 'Outros (Sem Cadastro)', 6, 'GRANDE');
insert into raca (id, nome, especie_id, tipo_porte) values (7, 'Outros (Sem Cadastro)', 7, 'MEDIO');
insert into raca (id, nome, especie_id, tipo_porte) values (8, 'Outros (Sem Cadastro)', 8, 'GRANDE');
insert into raca (id, nome, especie_id, tipo_porte) values (9, 'Outros (Sem Cadastro)', 9, 'MEDIO');
insert into raca (id, nome, especie_id, tipo_porte) values (10, 'Outros (Sem Cadastro)', 10, 'MEDIO');
insert into raca (id, nome, especie_id, tipo_porte) values (11, 'Outros (Sem Cadastro)', 11, 'INDEFINIDO');

--Raças Felinas
insert into raca (id, nome, especie_id, tipo_porte) values (12, 'Siamês', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (13, 'Persa', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (14, 'Exótico', 2, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (15, 'Himalaio', 2, 'PEQUENO');

--Raças Caninas
insert into raca (id, nome, especie_id, tipo_porte) values (16, 'Alano Espanhol', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (17, 'Airedale Terrier', 1, 'INDEFINIDO'); 
insert into raca (id, nome, especie_id, tipo_porte) values (18, 'American Staffordshire Terrier ', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (19, 'American Water Spaniel', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (20, 'Antigo Cão de Pastor Inglês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (21, 'Basset Azul da Gasconha', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (22, 'Basset Fulvo da Bretanha', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (23, 'Basset Hound', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (24, 'Beagle', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (25, 'Bearded Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (26, 'Maltês (Bichon Maltês)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (27, 'Bobtail', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (28, 'Border Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (29, 'Boston Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (30, 'Boxer', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (31, 'Bull Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (32, 'Bullmastiff', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (33, 'Bulldog', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (34, 'Cão de Montanha dos Pirinéus', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (35, 'Caniche', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (36, 'Chihuahua', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (37, 'Cirneco do Etna', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (38, 'Chow Chow', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (39, 'Cocker Spaniel (Americano ou Inglês)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (40, 'Dálmata', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (41, 'Dobermann', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (42, 'Dogue Alemão', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (43, 'Dogue Argentino', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (44, 'Dogue Canário', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (45, 'Fox Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (46, 'Foxhound', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (47, 'Galgo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (48, 'Golden Retriever', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (49, 'Gos d Atura', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (50, 'Husky Siberiano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (51, 'Laika', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (52, 'Labrador Retriever', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (53, 'Malamute-do-Alasca', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (54, 'Mastin dos Pirenéus', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (55, 'Mastin do Tibete', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (56, 'Mastin Espanhol', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (57, 'Mastín Napolitano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (58, 'Pastor Alemão', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (59, 'Pastor Belga', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (60, 'Pastor de Brie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (61, 'Pastor dos Pirenéus de Cara Rosa', 1, 'INDEFINIDO'); 
insert into raca (id, nome, especie_id, tipo_porte) values (62, 'Pequinês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (63, 'Perdigueiro', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (64, 'Pitbull', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (65, 'Podengo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (66, 'Pointer', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (67, 'Pug', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (68, 'Rhodesian Ridgeback', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (69, 'Rottweiler', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (70, 'Rough Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (71, 'Sabueso (Espanhol ou Italiano)', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (72, 'Saluki', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (73, 'Samoiedo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (74, 'São Bernardo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (75, 'Scottish Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (76, 'Setter Irlandés', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (77, 'Shar-Pei', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (78, 'Shiba Inu', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (79, 'Smooth Collie', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (80, 'Staffordshire Bull Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (81, 'Teckel', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (82, 'Terra-nova', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (83, 'Terrier Australiano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (84, 'Terrier Escocês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (85, 'Terrier Irlandês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (86, 'Terrier Japonês', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (87, 'Terrier Negro Russo', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (88, 'Terrier Norfolk', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (89, 'Terrier Norwich', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (90, 'Terrier Tibetano', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (91, 'Welhs Terrier', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (92, 'West Highland T.', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (93, 'Wolfspitz', 1, 'INDEFINIDO');
insert into raca (id, nome, especie_id, tipo_porte) values (94, 'Yorkshire Terrier', 1, 'INDEFINIDO');

--Raças Psitacídeos (aves)
insert into raca (id, nome, especie_id, tipo_porte) values (95, 'Papaguaio', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (96, 'Periquitoaustraliano', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (97, 'Cacatua', 5, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (98, 'Calopsita', 5, 'PEQUENO');

--Raças Roedores
insert into raca (id, nome, especie_id, tipo_porte) values (99, 'Hamster', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (100, 'Topolino', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (101, 'Gerbil - Esquilo da Mongólia', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (102, 'Camundongo', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (103, 'Mecol - Twister', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (104, 'Chinchila', 4, 'PEQUENO');
insert into raca (id, nome, especie_id, tipo_porte) values (105, 'Porquinho da Índia', 4, 'PEQUENO');

--Versão Futura

-- Cadastro de Vacinas
insert into vacina (id, nome) values (1, 'Outra (Sem Cadatro)');
insert into vacina (id, nome) values (2, 'Vermífugo');
insert into vacina (id, nome) values (3, 'V4');
insert into vacina (id, nome) values (4, 'Múltipla (V8, V10 ou Similar)');
insert into vacina (id, nome) values (5, 'Traqueobronquite Infecciosa (Tosse dos Canis)');
insert into vacina (id, nome) values (6, 'Anti-rábica');
insert into vacina (id, nome) values (7, 'Anti-pulgas');
insert into vacina (id, nome) values (8, 'Giardíase');
insert into vacina (id, nome) values (9, 'Leishmaniose Visceral Canina');
--SAVEPOINT vacinas_salvas;

-- Cadastro de Patologias
insert into patologia (id, nome, especie_id) values (1, 'Outra (Sem cadastro)', 1);
insert into patologia (id, nome, especie_id) values (2, 'Outra (Sem cadastro)', 2);
insert into patologia (id, nome, especie_id) values (3, 'Outra (Sem cadastro)', 3);
insert into patologia (id, nome, especie_id) values (4, 'Outra (Sem cadastro)', 4);
insert into patologia (id, nome, especie_id) values (5, 'Outra (Sem cadastro)', 5);
insert into patologia (id, nome, especie_id) values (6, 'Outra (Sem cadastro)', 6);
insert into patologia (id, nome, especie_id) values (7, 'Outra (Sem cadastro)', 7);
insert into patologia (id, nome, especie_id) values (8, 'Outra (Sem cadastro)', 8);
insert into patologia (id, nome, especie_id) values (9, 'Outra (Sem cadastro)', 9);
insert into patologia (id, nome, especie_id) values (10, 'Outra (Sem cadastro)', 10);

-- Patologias Caninas
insert into patologia (id, nome, especie_id) values (11, 'Babesiose - Piroplasmose', 1);
insert into patologia (id, nome, especie_id) values (12, 'Berne', 1);
insert into patologia (id, nome, especie_id) values (13, 'Cinomose - Esgana', 1);
insert into patologia (id, nome, especie_id) values (14, 'Cistite', 1);
insert into patologia (id, nome, especie_id) values (15, 'Coronavirose', 1);
insert into patologia (id, nome, especie_id) values (16, 'Dermatofitose', 1);
insert into patologia (id, nome, especie_id) values (17, 'Diabetes', 1);
insert into patologia (id, nome, especie_id) values (18, 'Dirofilariose', 1);
insert into patologia (id, nome, especie_id) values (19, 'Displasia da anca', 1);
insert into patologia (id, nome, especie_id) values (20, 'Doença de Lyme', 1);
insert into patologia (id, nome, especie_id) values (21, 'Giardíase', 1);
insert into patologia (id, nome, especie_id) values (22, 'Hepatite infecciosa canina', 1);
insert into patologia (id, nome, especie_id) values (23, 'Insuficiência renal', 1);
insert into patologia (id, nome, especie_id) values (24, 'Leishmaniose', 1);
insert into patologia (id, nome, especie_id) values (25, 'Leptospirose', 1);
insert into patologia (id, nome, especie_id) values (26, 'Obesidade', 1);
insert into patologia (id, nome, especie_id) values (27, 'Otite', 1);
insert into patologia (id, nome, especie_id) values (28, 'Parvovirose', 1);
insert into patologia (id, nome, especie_id) values (29, 'Raiva', 1);
insert into patologia (id, nome, especie_id) values (30, 'Sarna', 1);
insert into patologia (id, nome, especie_id) values (31, 'Tosse dos canis', 1);

--Patologias Felinos
insert into patologia (id, nome, especie_id) values (32, 'Peritonite Infecciosa Felina (PIF)', 2);
insert into patologia (id, nome, especie_id) values (33, 'Vírus da Imunodeficiência Felina (FIV)', 2);
insert into patologia (id, nome, especie_id) values (34, 'Rim Policístico (PKD)', 2);
insert into patologia (id, nome, especie_id) values (35, 'Vírus da Leucose Felina (FeLV)', 2);
insert into patologia (id, nome, especie_id) values (36, 'Coriza', 2);
insert into patologia (id, nome, especie_id) values (37, 'Panleucopénia', 2);
insert into patologia (id, nome, especie_id) values (38, 'Leucose', 2);
insert into patologia (id, nome, especie_id) values (39, 'Raiva', 2);
insert into patologia (id, nome, especie_id) values (40, 'Clamidiose', 2);
insert into patologia (id, nome, especie_id) values (41, 'Toxoplasmose', 2);
