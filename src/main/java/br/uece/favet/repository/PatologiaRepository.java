package br.uece.favet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.uece.favet.model.Patologia;

public class PatologiaRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Patologia porId(Long id) {
		return this.manager.find(Patologia.class, id);
	}

	/*
	public List<Patologia> porNome(String nome) {
		return this.manager.createQuery("from Patologia where upper(nome) like :nome", Patologia.class)
				.setParameter("nome", nome.toUpperCase() + "%")
				.getResultList();
    }
    
	
	public List<Patologia> patologias() {
		return this.manager.createQuery("from Patologia", Patologia.class)
				.getResultList();
	}
    
    */

	public List<Patologia> porEspecie(Long idEspecie) {
		return this.manager.createQuery("from Patologia where especie_id = :idEspecie order by nome", Patologia.class)
				.setParameter("idEspecie", idEspecie)
				.getResultList();
    }
	
	
}
