package br.uece.favet.repository;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.uece.favet.model.Especie;
import br.uece.favet.repository.filter.EspecieFilter;

public class EspecieRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Especie guardar(Especie especie){
		return this.manager.merge(especie);
	}
	
	public Especie porId(Long id) {
		return this.manager.find(Especie.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public Set<Especie> filtrados(EspecieFilter filtro) {

		Session session = this.manager.unwrap(Session.class);

		Criteria criteria = session.createCriteria(Especie.class);

		if (StringUtils.isNotBlank(filtro.getNome() )) {
			criteria.add(Restrictions.ilike("nome",
					filtro.getNome(), MatchMode.ANYWHERE));
		}
		
		return new LinkedHashSet<Especie>(criteria.addOrder(Order.asc("nome")).list());
	}
	
	
	public List<Especie> especies(){
		return this.manager.createQuery("from Especie", Especie.class)
				.getResultList();
	}
	
}
