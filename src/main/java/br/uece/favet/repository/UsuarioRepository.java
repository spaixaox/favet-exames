package br.uece.favet.repository;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.uece.favet.model.Usuario;
import br.uece.favet.repository.filter.UsuarioFilter;

public class UsuarioRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Usuario guardar(Usuario usuario){
		return this.manager.merge(usuario);
	}

	@SuppressWarnings("unchecked")
	public Set<Usuario> filtrados(UsuarioFilter filtro) {

		Session session = this.manager.unwrap(Session.class);

		Criteria criteria = session.createCriteria(Usuario.class);

		if (StringUtils.isNotBlank(filtro.getLogin() )) {
			criteria.add(Restrictions.ilike("login",
					filtro.getLogin(), MatchMode.ANYWHERE));
		}

		if (StringUtils.isNotBlank(filtro.getNome() )) {
			criteria.add(Restrictions.ilike("nome",
					filtro.getNome(), MatchMode.ANYWHERE));
		}
		
		if (StringUtils.isNotBlank(filtro.getEmail() )) {
			criteria.add(Restrictions.ilike("email",
					filtro.getEmail(), MatchMode.ANYWHERE));
		}
		
		return new LinkedHashSet<Usuario>(criteria.addOrder(Order.asc("nome")).list());
	}
	
	
	public Usuario porId(Long id) {
		return this.manager.find(Usuario.class, id);
	}

	public Usuario porEmailSenha(String email, String senha) {
		
		Usuario usuario = null;
		
		try {
			usuario = this.manager.createQuery("from Usuario where lower(email) = :email and senha = :senha", Usuario.class)
					  .setParameter("email", email.toLowerCase())
					  .setParameter("senha", senha)					  
					  .getSingleResult();			
		} catch (NoResultException e) {
			// nenhum usuário encontrado com o e-mail informado.
		}
		
		return usuario;
	}
	
	
	public Usuario porEmail(String email) {
		
		Usuario usuario = null;
		
		try {
			usuario = this.manager.createQuery("from Usuario where lower(email) = :email", Usuario.class)
					  .setParameter("email", email.toLowerCase()).getSingleResult();			
		} catch (NoResultException e) {
			// nenhum usuário encontrado com o e-mail informado.
		}
		
		return usuario;
	}
	
}
