package br.uece.favet.repository;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.uece.favet.model.Animal;
import br.uece.favet.repository.filter.AnimalFilter;

public class AnimalRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Animal guardar(Animal animal) {
		return this.manager.merge(animal);
	}
	
	@SuppressWarnings("unchecked")
	public Set<Animal> filtrados(AnimalFilter filtro) {

		Session session = this.manager.unwrap(Session.class);

		Criteria criteria = session.createCriteria(Animal.class)
				// fazemos uma associação (join) com proprietario e nomeamos como
				// "p"
				.createAlias("proprietario", "p", org.hibernate.sql.JoinType.RIGHT_OUTER_JOIN);

		if (StringUtils.isNotBlank(filtro.getNomeProprietario())) {
			criteria.add(Restrictions.ilike("p.nome",
					filtro.getNomeProprietario(), MatchMode.ANYWHERE));
		}

	    if (StringUtils.isNotBlank(filtro.getCpfCnpj())) {
			 criteria.add(Restrictions.eq("p.cpfCnpj",
			 filtro.getCpfCnpj())); 
		}
	    
		if (StringUtils.isNotBlank(filtro.getCodigoAnimal())) {			
			 criteria.add(Restrictions.eq("id",
			 Long.parseLong(filtro.getCodigoAnimal())  )); 
		}		

		if (StringUtils.isNotBlank(filtro.getCodigoLegadoAnimal())) {	
			 criteria.add(Restrictions.eq("codigoLegado",
			 filtro.getCodigoLegadoAnimal())); 
		}
				
		if (StringUtils.isNotBlank(filtro.getNomeAnimal())) {
			criteria.add(Restrictions.ilike("nome",
					filtro.getNomeAnimal(), MatchMode.ANYWHERE));
		}		
		
		return new LinkedHashSet<Animal>(criteria.addOrder(Order.asc("p.nome")).list());
	}
	
	
	public Animal porId(Long id) {
		return this.manager.find(Animal.class, id);
	}
	
	public List<Animal> animais(){
		return this.manager.createQuery("from Animal order by nome", Animal.class)
				.getResultList();
	}

	public List<Animal> porNome(String nome) {
			return this.manager.createQuery("from Animal where upper(nome) like :nome order by nome", Animal.class)
					.setParameter("nome", nome.toUpperCase() + "%")
					.getResultList();
	}
}
