package br.uece.favet.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.uece.favet.model.Consultorio;

public class ConsultorioRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Consultorio porId(Long id) {
		return this.manager.find(Consultorio.class, id);
	}
	
	public List<Consultorio> consultorios(){
		return this.manager.createQuery("from Consultorio order by nome", Consultorio.class)
				.getResultList();
	}	

}
