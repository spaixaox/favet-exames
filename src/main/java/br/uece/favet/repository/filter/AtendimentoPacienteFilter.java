package br.uece.favet.repository.filter;

import java.io.Serializable;
import java.util.Date;

import br.uece.favet.model.enums.StatusAtendimentoPaciente;
import br.uece.favet.model.enums.TipoAtendimentoPaciente;

public class AtendimentoPacienteFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date dataAtendimentoDe;
	private Date dataAtendimentoAte;	
	private Long codAtendimentoDe;
	private Long codAtendimentoAte;	
	private Long cpfCnpfProprietario;
	private String nomeProprietario;	
	private String nomeVeterinario;
	private String nomeConsultorio;
	private Long codigoAnimal;	
	private String nomeAnimal;
	private StatusAtendimentoPaciente[] statuses;
	private StatusAtendimentoPaciente statusEmEspera = StatusAtendimentoPaciente.ESPERA;	
	private TipoAtendimentoPaciente[] tiposAtendimentos;
	
	public Date getDataAtendimentoDe() {
		return dataAtendimentoDe;
	}
	public void setDataAtendimentoDe(Date dataAtendimentoDe) {
		this.dataAtendimentoDe = dataAtendimentoDe;
	}
	public Date getDataAtendimentoAte() {
		return dataAtendimentoAte;
	}
	public void setDataAtendimentoAte(Date dataAtendimentoAte) {
		this.dataAtendimentoAte = dataAtendimentoAte;
	}
	public Long getCodAtendimentoDe() {
		return codAtendimentoDe;
	}
	public void setCodAtendimentoDe(Long codAtendimentoDe) {
		this.codAtendimentoDe = codAtendimentoDe;
	}
	public Long getCodAtendimentoAte() {
		return codAtendimentoAte;
	}
	public void setCodAtendimentoAte(Long codAtendimentoAte) {
		this.codAtendimentoAte = codAtendimentoAte;
	}
	public Long getCpfCnpfProprietario() {
		return cpfCnpfProprietario;
	}
	public void setCpfCnpfProprietario(Long cpfCnpfProprietario) {
		this.cpfCnpfProprietario = cpfCnpfProprietario;
	}
	
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	public String getNomeVeterinario() {
		return nomeVeterinario;
	}
	public void setNomeVeterinario(String nomeVeterinario) {
		this.nomeVeterinario = nomeVeterinario;
	}
	public String getNomeConsultorio() {
		return nomeConsultorio;
	}
	public void setNomeConsultorio(String nomeConsultorio) {
		this.nomeConsultorio = nomeConsultorio;
	}
	
	public Long getCodigoAnimal() {
		return codigoAnimal;
	}
	public void setCodigoAnimal(Long codigoAnimal) {
		this.codigoAnimal = codigoAnimal;
	}
	public String getNomeAnimal() {
		return nomeAnimal;
	}
	public void setNomeAnimal(String nomeAnimal) {
		this.nomeAnimal = nomeAnimal;
	}
	public StatusAtendimentoPaciente[] getStatuses() {
		return statuses;
	}
	public void setStatuses(StatusAtendimentoPaciente[] statuses) {
		this.statuses = statuses;
	}
	
	public StatusAtendimentoPaciente getStatusEmEspera() {
		return statusEmEspera;
	}
	public void setStatusEmEspera(StatusAtendimentoPaciente statusEmEspera) {
		this.statusEmEspera = statusEmEspera;
	}
	public TipoAtendimentoPaciente[] getTiposAtendimentos() {
		return tiposAtendimentos;
	}
	public void setTiposAtendimentos(TipoAtendimentoPaciente[] tiposAtendimentos) {
		this.tiposAtendimentos = tiposAtendimentos;
	}	
	
}
