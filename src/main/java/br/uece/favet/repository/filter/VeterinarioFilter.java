package br.uece.favet.repository.filter;

import java.io.Serializable;

public class VeterinarioFilter implements Serializable {

	private static final long serialVersionUID = 1L;

	private String crmv;
	private String nome;

	public String getCrmv() {
		return crmv;
	}
	public void setCrmv(String crmv) {
		this.crmv = crmv;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
