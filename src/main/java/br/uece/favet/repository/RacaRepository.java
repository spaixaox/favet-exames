package br.uece.favet.repository;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.uece.favet.model.Raca;
import br.uece.favet.repository.filter.RacaFilter;

public class RacaRepository implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;
	
	public Raca guardar(Raca raca){
		return this.manager.merge(raca);
	}
	
	public Raca porId(Long id) {
		return this.manager.find(Raca.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public Set<Raca> filtrados(RacaFilter filtro) {

		Session session = this.manager.unwrap(Session.class);

		Criteria criteria = session.createCriteria(Raca.class)
				// fazemos uma associação (join) com animal e nomeamos como "a"
			.createAlias("especie", "e");
				

		if (StringUtils.isNotBlank(filtro.getNomeRaca() )) {
			criteria.add(Restrictions.ilike("nome",
					filtro.getNomeRaca(), MatchMode.ANYWHERE));
		}
		
		if (StringUtils.isNotBlank(filtro.getNomeEspecie() )) {
			criteria.add(Restrictions.ilike("e.nome",
					filtro.getNomeEspecie(), MatchMode.ANYWHERE));
		}		
		
		return new LinkedHashSet<Raca>(criteria.addOrder(Order.asc("nome")).list());
	}
	
	
	public List<Raca> racas(){
		return this.manager.createQuery("from Raca", Raca.class)
				.getResultList();
	}
	
	public List<Raca> racasPorEspecie(Long idEspecie) {
		return this.manager.createQuery("from Raca where especie.id = :idEspecie ", Raca.class)
				.setParameter("idEspecie", idEspecie)				
				.getResultList();
	}	
	
}
