package br.uece.favet.service;

import java.io.Serializable;

import javax.inject.Inject;

import br.uece.favet.model.Raca;
import br.uece.favet.repository.RacaRepository;
import br.uece.favet.util.jpa.Transactional;

public class CadastroRacaService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	RacaRepository racaRepository;
	
	@Transactional
	public Raca salvar(Raca raca){

		try {
			raca = this.racaRepository.guardar(raca);
		} catch (NegocioException e) {
			throw new NegocioException("Erro ao tentar salvar Raça: ");
		}

		return raca;
	}

}
