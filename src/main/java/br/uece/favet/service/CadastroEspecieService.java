package br.uece.favet.service;

import java.io.Serializable;

import javax.inject.Inject;

import br.uece.favet.model.Especie;
import br.uece.favet.repository.EspecieRepository;
import br.uece.favet.util.jpa.Transactional;

public class CadastroEspecieService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	EspecieRepository especieRepository;
	
	@Transactional
	public Especie salvar(Especie especie){

		try {
			especie = this.especieRepository.guardar(especie);
		} catch (NegocioException e) {
			throw new NegocioException("Erro ao tentar salvar Espécie: ");
		}

		return especie;
	}

}
