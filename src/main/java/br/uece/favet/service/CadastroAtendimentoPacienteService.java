package br.uece.favet.service;

import java.io.Serializable;
import java.util.Date;

import javax.inject.Inject;

import br.uece.favet.model.Animal;
import br.uece.favet.model.AtendimentoPaciente;
import br.uece.favet.model.Usuario;
import br.uece.favet.repository.AnimalRepository;
import br.uece.favet.repository.AtendimentoPacienteRepository;
import br.uece.favet.util.jpa.Transactional;

public class CadastroAtendimentoPacienteService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	AtendimentoPacienteRepository atendimentoPacienteRepository;
	
	@Inject
	AnimalRepository animalRepository;
	
	@Transactional
	public AtendimentoPaciente salvar(AtendimentoPaciente atendimentoPaciente, Usuario usuarioLogado){
	
		if (atendimentoPaciente.isNovo()){
			atendimentoPaciente.setDataCriacao(new Date());
			atendimentoPaciente.setDataHoraCriacao(new Date());
			
			if (usuarioLogado != null){
				atendimentoPaciente.setUsuarioCriou(usuarioLogado);
				atendimentoPaciente.setUsuarioAtualizou(usuarioLogado);				
			}
			//Se respeitar os requisitos, alterar status
			//entradaMercadoria.setStatus(status);
		} else {
			atendimentoPaciente.setDataHoraAtualizacao(new Date());

			if (usuarioLogado != null){
				atendimentoPaciente.setUsuarioAtualizou(usuarioLogado);				
			}			
		}
		
		//Atualiza dataNascimento e peso do animal
		Animal animal = animalRepository.porId(atendimentoPaciente.getAnimal().getId());
        animal.setDataNascimento(atendimentoPaciente.getAnimal().getDataNascimento());        
        if (atendimentoPaciente.getPeso() != null){
        	animal.setPeso(atendimentoPaciente.getPeso().doubleValue());
        }
        atendimentoPaciente.setAnimal(animal);
		atendimentoPaciente.setDataHoraAtualizacao(new Date());		
		
		atendimentoPaciente = this.atendimentoPacienteRepository.guardar(atendimentoPaciente);
		return atendimentoPaciente;
		
	}
}
