package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.Consultorio;

import br.uece.favet.repository.ConsultorioRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass=Consultorio.class)
public class ConsultorioConverter implements Converter {

	//@Inject
	private ConsultorioRepository consultorioRepository;
	
	public ConsultorioConverter() {
		this.consultorioRepository = (ConsultorioRepository) CDIServiceLocator.getBean(ConsultorioRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Consultorio retorno = null;

		if (value != null) {
			retorno = this.consultorioRepository.porId(new Long(value));
		}

		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			return ((Consultorio) value).getId().toString();
		}
		return "";
	}

}