package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.Veterinario;
import br.uece.favet.repository.VeterinarioRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = Veterinario.class)
public class VeterinarioConverter implements Converter {

	//@Inject
	//private VeterinarioRepository veterinarioRepository;
	private VeterinarioRepository veterinarioRepository;
	
	public VeterinarioConverter() {
		veterinarioRepository = CDIServiceLocator.getBean(VeterinarioRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Veterinario retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = veterinarioRepository.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Veterinario veterinario = (Veterinario) value;
			return veterinario.getId() == null ? null : veterinario.getId().toString();
		}
		
		return "";
	}

}
