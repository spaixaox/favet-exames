package br.uece.favet.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.uece.favet.model.Especie;
import br.uece.favet.repository.EspecieRepository;
import br.uece.favet.util.cdi.CDIServiceLocator;

@FacesConverter(forClass = Especie.class)
public class EspecieConverter implements Converter {

	//@Inject
	//private EspecieRepository especieRepository;
	private EspecieRepository especieRepository;
	
	public EspecieConverter() {
		especieRepository = CDIServiceLocator.getBean(EspecieRepository.class);
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Especie retorno = null;
		
		if (value != null) {
			Long id = new Long(value);
			retorno = especieRepository.porId(id);
		}
		
		return retorno;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Especie especie = (Especie) value;
			return especie.getId() == null ? null : especie.getId().toString();
		}
		
		return "";
	}

}
