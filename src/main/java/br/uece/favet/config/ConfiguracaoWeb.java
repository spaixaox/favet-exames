package br.uece.favet.config;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class ConfiguracaoWeb implements Serializable{

	private static final long serialVersionUID = 1L;

	private String sistemaNome;
	private String sistemaVersao;
	private String bancoVersao;

	public ConfiguracaoWeb() {
		super();
		carregaConfigs();
	}

	private void carregaConfigs(){

		try {
			
			if (this.sistemaNome == null) {
				Properties props = new Properties();				
				props.load(getClass().getResourceAsStream("/sistema.properties"));
				this.sistemaNome = props.getProperty("sistema.nome");
				this.sistemaVersao = props.getProperty("sistema.versao");	
				this.bancoVersao = props.getProperty("banco.versao");
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro ao tentar carrregar arquivo sistema.properties: "+e.getMessage());
		}
	}

	public String getSistemaNome() {
		return sistemaNome;
	}

	public String getSistemaVersao() {
		return sistemaVersao;
	}

	public String getBancoVersao() {
		return bancoVersao;
	}

}
