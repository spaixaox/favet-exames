package br.uece.favet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.uece.favet.model.enums.TipoPorte;

@Entity
@Table(name="raca")
public class Raca implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="raca_id_seq")
    @SequenceGenerator(name="raca_id_seq", sequenceName="raca_id_seq", allocationSize=1)
	private Long id;

	@NotNull
	@Column(name = "nome", nullable = false, length=50)	
	private String nome;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "especie_id", nullable = false, foreignKey = @ForeignKey(name = "fk_raca__especie__especie_id"))
	private Especie especie;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_porte", length = 10)	
	private TipoPorte tipoPorte;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public TipoPorte getTipoPorte() {
		return tipoPorte;
	}

	public void setTipoPorte(TipoPorte tipoPorte) {
		this.tipoPorte = tipoPorte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((especie == null) ? 0 : especie.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result
				+ ((tipoPorte == null) ? 0 : tipoPorte.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Raca other = (Raca) obj;
		if (especie == null) {
			if (other.especie != null)
				return false;
		} else if (!especie.equals(other.especie))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (tipoPorte != other.tipoPorte)
			return false;
		return true;
	}

}
