package br.uece.favet.model.enums;

public enum TipoPessoa {

	FISICA, JURIDICA;
	
}
