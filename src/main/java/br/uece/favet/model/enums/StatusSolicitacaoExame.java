package br.uece.favet.model.enums;

public enum StatusSolicitacaoExame {
	
	AGUARDANDO_MATERIAL("Aguardando Material"),
	MATERIAL_RECEBIDO("Material Recebido"),
	ANALISE("Em Análise"),
	CONCLUIDO("Concluído");	
	
	private String descricao;
	
	StatusSolicitacaoExame(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
