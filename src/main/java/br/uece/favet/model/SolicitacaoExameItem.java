package br.uece.favet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="solicitacao_exame_item")
public class SolicitacaoExameItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="solicitacao_exame_item_id_seq")
    @SequenceGenerator(name="solicitacao_exame_item_id_seq", sequenceName="solicitacao_exame_item_id_seq", allocationSize=1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "solicitacao_exame_id", nullable = false, foreignKey = @ForeignKey(name = "fk_solicitacao_exame_item__solicitacao_exame_id"))
	private SolicitacaoExame solicitacaoExame;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "categoria_exame_id", nullable = false, foreignKey = @ForeignKey(name = "fk_solicitacao_exame_item__categoria_exame_id"))	
	private CategoriaExame categoriaExame;    

	@NotNull
	@ManyToOne
	@JoinColumn(name = "tipo_exame_id", nullable = false, foreignKey = @ForeignKey(name = "fk_solicitacao_exame_item__tipo_exame_id"))	
	private TipoExame tipoExame;

	@Column(nullable = true, length = 50)	
	private String outroTipoExame;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SolicitacaoExame getSolicitacaoExame() {
		return solicitacaoExame;
	}

	public void setSolicitacaoExame(SolicitacaoExame solicitacaoExame) {
		this.solicitacaoExame = solicitacaoExame;
	}

	public CategoriaExame getCategoriaExame() {
		return categoriaExame;
	}

	public void setCategoriaExame(CategoriaExame categoriaExame) {
		this.categoriaExame = categoriaExame;
	}

	public TipoExame getTipoExame() {
		return tipoExame;
	}

	public void setTipoExame(TipoExame tipoExame) {
		this.tipoExame = tipoExame;
	}

	public String getOutroTipoExame() {
		return outroTipoExame;
	}

	public void setOutroTipoExame(String outroTipoExame) {
		this.outroTipoExame = outroTipoExame;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoriaExame == null) ? 0 : categoriaExame.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((outroTipoExame == null) ? 0 : outroTipoExame.hashCode());
		result = prime
				* result
				+ ((solicitacaoExame == null) ? 0 : solicitacaoExame.hashCode());
		result = prime * result
				+ ((tipoExame == null) ? 0 : tipoExame.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitacaoExameItem other = (SolicitacaoExameItem) obj;
		if (categoriaExame == null) {
			if (other.categoriaExame != null)
				return false;
		} else if (!categoriaExame.equals(other.categoriaExame))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (outroTipoExame == null) {
			if (other.outroTipoExame != null)
				return false;
		} else if (!outroTipoExame.equals(other.outroTipoExame))
			return false;
		if (solicitacaoExame == null) {
			if (other.solicitacaoExame != null)
				return false;
		} else if (!solicitacaoExame.equals(other.solicitacaoExame))
			return false;
		if (tipoExame == null) {
			if (other.tipoExame != null)
				return false;
		} else if (!tipoExame.equals(other.tipoExame))
			return false;
		return true;
	}	
	
}
