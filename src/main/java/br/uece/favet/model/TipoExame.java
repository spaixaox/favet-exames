package br.uece.favet.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tipo_exame")
public class TipoExame implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="tipo_exame_id_seq")
    @SequenceGenerator(name="tipo_exame_id_seq", sequenceName="tipo_exame_id_seq", allocationSize=1)
	private Long id;

	@NotNull
	@Column(length=100, nullable=false)
	private String nome;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "categoria_exame_id", nullable = false, foreignKey = @ForeignKey(name = "fk_tipo_exame__categoria_exame_id"))
	private CategoriaExame categoriaExame;
	
	@NotNull
	@Column(nullable=false)	
	private Boolean ativo = true;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public CategoriaExame getCategoriaExame() {
		return categoriaExame;
	}

	public void setCategoriaExame(CategoriaExame categoriaExame) {
		this.categoriaExame = categoriaExame;
	}

	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result
				+ ((categoriaExame == null) ? 0 : categoriaExame.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoExame other = (TipoExame) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (categoriaExame == null) {
			if (other.categoriaExame != null)
				return false;
		} else if (!categoriaExame.equals(other.categoriaExame))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

}
