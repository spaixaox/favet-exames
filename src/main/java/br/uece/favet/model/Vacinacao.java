package br.uece.favet.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="vacinacao")
public class Vacinacao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="vacinacao_id_seq")
    @SequenceGenerator(name="vacinacao_id_seq", sequenceName="vacinacao_id_seq", allocationSize=1)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "animal_id", nullable=false, foreignKey = @ForeignKey(name = "fk_vacinacao__animal__animal_id"))	
	private Animal animal;
	
	@ManyToOne
	@JoinColumn(name = "vacina_id", foreignKey = @ForeignKey(name = "fk_vacinacao__vacina__vacina_id"))	
	private Vacina vacina;
	
	@Column(name="outra_vacina", length=50)
	private String outraVacina;	//campo temporário até ficar pronto o cadastro de Vacina

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_criacao", nullable=false)
	private Date dataCriacao;	
	
	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "data_programacao", nullable=false)
	private Date dataProgramacao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_aplicacao")
	private Date dataAplicacao;		
	
	@Column(columnDefinition = "text")
	private String observacao;
	
	@ManyToOne
	@JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_vacinacao__usuario__usuario_id"))	
	private Usuario usuario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Vacina getVacina() {
		return vacina;
	}

	public void setVacina(Vacina vacina) {
		this.vacina = vacina;
	}

	public String getOutraVacina() {
		return outraVacina;
	}

	public void setOutraVacina(String outraVacina) {
		this.outraVacina = outraVacina;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataProgramacao() {
		return dataProgramacao;
	}

	public void setDataProgramacao(Date dataProgramacao) {
		this.dataProgramacao = dataProgramacao;
	}

	public Date getDataAplicacao() {
		return dataAplicacao;
	}

	public void setDataAplicacao(Date dataAplicacao) {
		this.dataAplicacao = dataAplicacao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((animal == null) ? 0 : animal.hashCode());
		result = prime * result
				+ ((dataAplicacao == null) ? 0 : dataAplicacao.hashCode());
		result = prime * result
				+ ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		result = prime * result
				+ ((dataProgramacao == null) ? 0 : dataProgramacao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result
				+ ((outraVacina == null) ? 0 : outraVacina.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		result = prime * result + ((vacina == null) ? 0 : vacina.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vacinacao other = (Vacinacao) obj;
		if (animal == null) {
			if (other.animal != null)
				return false;
		} else if (!animal.equals(other.animal))
			return false;
		if (dataAplicacao == null) {
			if (other.dataAplicacao != null)
				return false;
		} else if (!dataAplicacao.equals(other.dataAplicacao))
			return false;
		if (dataCriacao == null) {
			if (other.dataCriacao != null)
				return false;
		} else if (!dataCriacao.equals(other.dataCriacao))
			return false;
		if (dataProgramacao == null) {
			if (other.dataProgramacao != null)
				return false;
		} else if (!dataProgramacao.equals(other.dataProgramacao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (outraVacina == null) {
			if (other.outraVacina != null)
				return false;
		} else if (!outraVacina.equals(other.outraVacina))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		if (vacina == null) {
			if (other.vacina != null)
				return false;
		} else if (!vacina.equals(other.vacina))
			return false;
		return true;
	}	

}
