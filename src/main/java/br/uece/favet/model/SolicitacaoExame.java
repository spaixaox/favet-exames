package br.uece.favet.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import br.uece.favet.model.enums.StatusSolicitacaoExame;

@Entity
@Table(name="solicitacao_exame")
public class SolicitacaoExame implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator="solicitacao_exame_id_seq")
    @SequenceGenerator(name="solicitacao_exame_id_seq", sequenceName="solicitacao_exame_id_seq", allocationSize=1)
	private Long id;

	@Column(name = "codigo_exame", length=10)
	private String codigoExame;	

	@Column(length=50)
	private String responsavel;	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_coleta")	
	private Date dataHoraColeta;	
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 20)
	private StatusSolicitacaoExame status = StatusSolicitacaoExame.AGUARDANDO_MATERIAL;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "animal_id", nullable = false, foreignKey = @ForeignKey(name = "fk_solicitacao_exame__animal_id"))	
	private Animal animal;

	@ManyToOne
	@JoinColumn(name = "veterinario_id", nullable=true, foreignKey = @ForeignKey(name = "fk_solicitacao_exame__veterinario_id"))	
	private Veterinario solicitante;

	@ManyToOne
	@JoinColumn(name = "laboratorio_id", nullable=true, foreignKey = @ForeignKey(name = "fk_solicitacao_exame__laboratorio_id"))	
	private Laboratorio laboratorio;

	@ManyToOne
	@JoinColumn(name = "usuario_criou_id", foreignKey = @ForeignKey(name = "fk_solicitacao_exame__usuario_criou_id"))	
	private Usuario usuarioCriou;
	
	@ManyToOne
	@JoinColumn(name = "usuario_recebeu_id", foreignKey = @ForeignKey(name = "fk_solicitacao_exame__usuario_recebeu_id"))	
	private Usuario usuarioRecebeu;

	@ManyToOne
	@JoinColumn(name = "usuario_concluiu_id", foreignKey = @ForeignKey(name = "fk_solicitacao_exame__usuario_concluiu_id"))	
	private Usuario usuarioConclui;
	
	@ManyToOne
	@JoinColumn(name = "usuario_atualizou_id", foreignKey = @ForeignKey(name = "fk_solicitacao_exame__usuario_atualizou_id"))	
	private Usuario usuarioAtualizou;	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_criacao")	
	private Date dataHoraCriacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_recebimento")	
	private Date dataHoraRecebimento;	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_processamento")	
	private Date dataHoraProcessamento;	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_conclusao")	
	private Date dataHoraConclusao;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora_atualizacao")	
	private Date dataHoraAtualizacao;

	@OneToMany(mappedBy = "solicitacaoExame", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)	
	private List<SolicitacaoExameItem> solicitacaoExameItem  = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoExame() {
		return codigoExame;
	}

	public void setCodigoExame(String codigoExame) {
		this.codigoExame = codigoExame;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public Date getDataHoraColeta() {
		return dataHoraColeta;
	}

	public void setDataHoraColeta(Date dataHoraColeta) {
		this.dataHoraColeta = dataHoraColeta;
	}

	public StatusSolicitacaoExame getStatus() {
		return status;
	}

	public void setStatus(StatusSolicitacaoExame status) {
		this.status = status;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Veterinario getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Veterinario solicitante) {
		this.solicitante = solicitante;
	}

	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

	public Usuario getUsuarioCriou() {
		return usuarioCriou;
	}

	public void setUsuarioCriou(Usuario usuarioCriou) {
		this.usuarioCriou = usuarioCriou;
	}

	public Usuario getUsuarioRecebeu() {
		return usuarioRecebeu;
	}

	public void setUsuarioRecebeu(Usuario usuarioRecebeu) {
		this.usuarioRecebeu = usuarioRecebeu;
	}

	public Usuario getUsuarioConclui() {
		return usuarioConclui;
	}

	public void setUsuarioConclui(Usuario usuarioConclui) {
		this.usuarioConclui = usuarioConclui;
	}

	public Usuario getUsuarioAtualizou() {
		return usuarioAtualizou;
	}

	public void setUsuarioAtualizou(Usuario usuarioAtualizou) {
		this.usuarioAtualizou = usuarioAtualizou;
	}

	public Date getDataHoraCriacao() {
		return dataHoraCriacao;
	}

	public void setDataHoraCriacao(Date dataHoraCriacao) {
		this.dataHoraCriacao = dataHoraCriacao;
	}

	public Date getDataHoraRecebimento() {
		return dataHoraRecebimento;
	}

	public void setDataHoraRecebimento(Date dataHoraRecebimento) {
		this.dataHoraRecebimento = dataHoraRecebimento;
	}

	public Date getDataHoraProcessamento() {
		return dataHoraProcessamento;
	}

	public void setDataHoraProcessamento(Date dataHoraProcessamento) {
		this.dataHoraProcessamento = dataHoraProcessamento;
	}

	public Date getDataHoraConclusao() {
		return dataHoraConclusao;
	}

	public void setDataHoraConclusao(Date dataHoraConclusao) {
		this.dataHoraConclusao = dataHoraConclusao;
	}

	public Date getDataHoraAtualizacao() {
		return dataHoraAtualizacao;
	}

	public void setDataHoraAtualizacao(Date dataHoraAtualizacao) {
		this.dataHoraAtualizacao = dataHoraAtualizacao;
	}

	public List<SolicitacaoExameItem> getSolicitacaoExameItem() {
		return solicitacaoExameItem;
	}

	public void setSolicitacaoExameItem(
			List<SolicitacaoExameItem> solicitacaoExameItem) {
		this.solicitacaoExameItem = solicitacaoExameItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((animal == null) ? 0 : animal.hashCode());
		result = prime * result
				+ ((codigoExame == null) ? 0 : codigoExame.hashCode());
		result = prime
				* result
				+ ((dataHoraAtualizacao == null) ? 0 : dataHoraAtualizacao
						.hashCode());
		result = prime * result
				+ ((dataHoraColeta == null) ? 0 : dataHoraColeta.hashCode());
		result = prime
				* result
				+ ((dataHoraConclusao == null) ? 0 : dataHoraConclusao
						.hashCode());
		result = prime * result
				+ ((dataHoraCriacao == null) ? 0 : dataHoraCriacao.hashCode());
		result = prime
				* result
				+ ((dataHoraProcessamento == null) ? 0 : dataHoraProcessamento
						.hashCode());
		result = prime
				* result
				+ ((dataHoraRecebimento == null) ? 0 : dataHoraRecebimento
						.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((laboratorio == null) ? 0 : laboratorio.hashCode());
		result = prime * result
				+ ((responsavel == null) ? 0 : responsavel.hashCode());
		result = prime
				* result
				+ ((solicitacaoExameItem == null) ? 0 : solicitacaoExameItem
						.hashCode());
		result = prime * result
				+ ((solicitante == null) ? 0 : solicitante.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime
				* result
				+ ((usuarioAtualizou == null) ? 0 : usuarioAtualizou.hashCode());
		result = prime * result
				+ ((usuarioConclui == null) ? 0 : usuarioConclui.hashCode());
		result = prime * result
				+ ((usuarioCriou == null) ? 0 : usuarioCriou.hashCode());
		result = prime * result
				+ ((usuarioRecebeu == null) ? 0 : usuarioRecebeu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitacaoExame other = (SolicitacaoExame) obj;
		if (animal == null) {
			if (other.animal != null)
				return false;
		} else if (!animal.equals(other.animal))
			return false;
		if (codigoExame == null) {
			if (other.codigoExame != null)
				return false;
		} else if (!codigoExame.equals(other.codigoExame))
			return false;
		if (dataHoraAtualizacao == null) {
			if (other.dataHoraAtualizacao != null)
				return false;
		} else if (!dataHoraAtualizacao.equals(other.dataHoraAtualizacao))
			return false;
		if (dataHoraColeta == null) {
			if (other.dataHoraColeta != null)
				return false;
		} else if (!dataHoraColeta.equals(other.dataHoraColeta))
			return false;
		if (dataHoraConclusao == null) {
			if (other.dataHoraConclusao != null)
				return false;
		} else if (!dataHoraConclusao.equals(other.dataHoraConclusao))
			return false;
		if (dataHoraCriacao == null) {
			if (other.dataHoraCriacao != null)
				return false;
		} else if (!dataHoraCriacao.equals(other.dataHoraCriacao))
			return false;
		if (dataHoraProcessamento == null) {
			if (other.dataHoraProcessamento != null)
				return false;
		} else if (!dataHoraProcessamento.equals(other.dataHoraProcessamento))
			return false;
		if (dataHoraRecebimento == null) {
			if (other.dataHoraRecebimento != null)
				return false;
		} else if (!dataHoraRecebimento.equals(other.dataHoraRecebimento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (laboratorio == null) {
			if (other.laboratorio != null)
				return false;
		} else if (!laboratorio.equals(other.laboratorio))
			return false;
		if (responsavel == null) {
			if (other.responsavel != null)
				return false;
		} else if (!responsavel.equals(other.responsavel))
			return false;
		if (solicitacaoExameItem == null) {
			if (other.solicitacaoExameItem != null)
				return false;
		} else if (!solicitacaoExameItem.equals(other.solicitacaoExameItem))
			return false;
		if (solicitante == null) {
			if (other.solicitante != null)
				return false;
		} else if (!solicitante.equals(other.solicitante))
			return false;
		if (status != other.status)
			return false;
		if (usuarioAtualizou == null) {
			if (other.usuarioAtualizou != null)
				return false;
		} else if (!usuarioAtualizou.equals(other.usuarioAtualizou))
			return false;
		if (usuarioConclui == null) {
			if (other.usuarioConclui != null)
				return false;
		} else if (!usuarioConclui.equals(other.usuarioConclui))
			return false;
		if (usuarioCriou == null) {
			if (other.usuarioCriou != null)
				return false;
		} else if (!usuarioCriou.equals(other.usuarioCriou))
			return false;
		if (usuarioRecebeu == null) {
			if (other.usuarioRecebeu != null)
				return false;
		} else if (!usuarioRecebeu.equals(other.usuarioRecebeu))
			return false;
		return true;
	}

}
