package br.uece.favet.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Proprietario;
import br.uece.favet.repository.ProprietarioRepository;
import br.uece.favet.repository.filter.ProprietarioFilter;

@Named
@ViewScoped
public class PesquisaProprietarioSemAnimalBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ProprietarioRepository proprietarioRepository;

	private ProprietarioFilter filtro;

	private Set<Proprietario> proprietariosFiltrados;

	public PesquisaProprietarioSemAnimalBean() {
		filtro = new ProprietarioFilter();
		proprietariosFiltrados = new HashSet<Proprietario>();
	}

	public void pesquisar() {
		proprietariosFiltrados = proprietarioRepository.filtrados(filtro);
        System.out.println("proprietariosFiltrados.size(): "+proprietariosFiltrados.size());		
	}

	public Set<Proprietario> getProprietariosFiltrados() {
		return proprietariosFiltrados;
	}

	public ProprietarioFilter getFiltro() {
		return filtro;
	}

}
