package br.uece.favet.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Animal;
import br.uece.favet.repository.AnimalRepository;
import br.uece.favet.repository.filter.AnimalFilter;
import br.uece.favet.security.Seguranca;

@Named
@ViewScoped
public class PesquisaAnimalBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Seguranca seguranca;	
	
	@Inject
	private AnimalRepository animalRepository;

	private AnimalFilter filtro;

	private Set<Animal> animaisFiltrados;

	public PesquisaAnimalBean() {
		filtro = new AnimalFilter();
		animaisFiltrados = new HashSet<Animal>();
	}

	public void pesquisar() {
		animaisFiltrados = animalRepository.filtrados(filtro);
	}

	public Set<Animal> getAnimaisFiltrados() {
		return animaisFiltrados;
	}

	public AnimalFilter getFiltro() {
		return filtro;
	}
	
	public boolean isUsuarioVeterinario() {
		return seguranca.getTipoGrupoUsuario().toUpperCase().equals("VETERINARIO");
	}

}
