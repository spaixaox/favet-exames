package br.uece.favet.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.AtendimentoPaciente;
import br.uece.favet.model.enums.StatusAtendimentoPaciente;
import br.uece.favet.model.enums.TipoAtendimentoPaciente;
import br.uece.favet.repository.AtendimentoPacienteRepository;
import br.uece.favet.repository.filter.AtendimentoPacienteFilter;
import br.uece.favet.security.Seguranca;

@Named
@ViewScoped
public class PesquisaAtendimentoPacienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private AtendimentoPacienteRepository atendimentoPacienteRepository;
	
	@Inject
	private Seguranca seguranca;	

	private AtendimentoPacienteFilter filtro;

	private List<AtendimentoPaciente> atendimentosFiltrados;
	
	private String totaisAtendimentos;
	private int totalEmEspera = 0;
	private int totalEmAtendimento = 0;
	private int totalAtendido = 0;
	private int totalCancelado = 0;

	public PesquisaAtendimentoPacienteBean() {
		filtro = new AtendimentoPacienteFilter();
		atendimentosFiltrados = new ArrayList<>();
		//createHorizontalBarModel();
	}
	
	public String getTotaisAtendimentos() {
		return totaisAtendimentos;
	}

	public void pesquisar() {
		atendimentosFiltrados = atendimentoPacienteRepository.filtrados(filtro);
		carregarTotaisAtendimentos();
	}
	
	private String pegaParametro(){
		String tipoAtendimento = null;
		
		try {
			tipoAtendimento = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("tipoatendimento");			
		} catch (Exception e) {
			
		}

		if (tipoAtendimento == null){
			tipoAtendimento = "todos";
		}
		
		System.out.println("tipoAtendimento: "+tipoAtendimento);		
		
		return tipoAtendimento;
	}
	
	public void pesquisarEmEspera() {
		
		String paramTipoAtendimento = pegaParametro();
		switch (paramTipoAtendimento) {
 		case "clinico":
 			TipoAtendimentoPaciente tipoAtendimentosClinico[] = {TipoAtendimentoPaciente.CLINICO, TipoAtendimentoPaciente.RETORNO}; 			
 			filtro.setTiposAtendimentos(tipoAtendimentosClinico);
			break;
		case "cirurgico":
 			TipoAtendimentoPaciente tipoAtendimentoCirurgico[] = {TipoAtendimentoPaciente.CIRURGICO, TipoAtendimentoPaciente.AVALIACAO_CIRURGICA, TipoAtendimentoPaciente.RETORNO_CIRURGICO}; 			
 			filtro.setTiposAtendimentos(tipoAtendimentoCirurgico); 			
			break;
		case "ultrassom":
 			TipoAtendimentoPaciente tipoAtendimentoUltrassom[] = {TipoAtendimentoPaciente.EXAME}; 			
 			filtro.setTiposAtendimentos(tipoAtendimentoUltrassom); 			
			break;			
		default:
			break;
		}
		
		filtro.setDataAtendimentoDe(new Date());
		filtro.setDataAtendimentoAte(new Date());
		
		atendimentosFiltrados = atendimentoPacienteRepository.filtradosEmEspera(filtro);
	}
	
	public StatusAtendimentoPaciente[] getStatuses() {
		return StatusAtendimentoPaciente.values();
	}

	public TipoAtendimentoPaciente[] getTiposAtendimentos() {
		return TipoAtendimentoPaciente.values();
	}	
	
	public List<AtendimentoPaciente> getAtendimentosFiltrados() {
		return atendimentosFiltrados;
	}

	public AtendimentoPacienteFilter getFiltro() {
		return filtro;
	}
	
	private void carregarTotaisAtendimentos(){

		totalEmEspera = 0;
		totalEmAtendimento = 0;
		totalAtendido = 0;
		totalCancelado = 0;
		
		for (AtendimentoPaciente atendimento : this.atendimentosFiltrados) {

			if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.ESPERA.toString())){
				totalEmEspera = totalEmEspera + 1;
			} else if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.ATENDIMENTO.toString())){
				totalEmAtendimento++;
			} else if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.ATENDIDO.toString())){
				totalAtendido++;				
			} else if (atendimento.getStatus().toString().equals(StatusAtendimentoPaciente.CANCELADO.toString())){
				totalCancelado++;
			}
		}
		
		this.totaisAtendimentos = "Em Espera: "+String.valueOf(totalEmEspera)+"     |"+
				                  "Em Atendimento: "+String.valueOf(totalEmAtendimento)+"      |"+
				                  "Atendido(s): "+String.valueOf(totalAtendido)+"     |"+
				                  "Cancelado(s): "+String.valueOf(totalCancelado);

	}

	public int getTotalEmEspera() {
		return totalEmEspera;
	}

	public int getTotalEmAtendimento() {
		return totalEmAtendimento;
	}

	public int getTotalAtendido() {
		return totalAtendido;
	}

	public int getTotalCancelado() {
		return totalCancelado;
	}
	
	public boolean isUsuarioVeterinario() {
		return seguranca.getTipoGrupoUsuario().toUpperCase().equals("VETERINARIO");
	}
	
	public boolean isUsuarioVeterinarioCirurgiao(){
		if (isUsuarioVeterinario()){
			return seguranca.getVeterinarioUsuario().isCirurgiao();
		} 
		return false;
	}	
	
	public boolean isUsuarioVeterinarioClinico(){
		if (isUsuarioVeterinario()){
			return seguranca.getVeterinarioUsuario().getClinico();
		} 
		return false;
	}	
	
	public boolean isUsuarioVeterinarioLaudista(){
		if (isUsuarioVeterinario()){
			return seguranca.getVeterinarioUsuario().getFazExames();
		} 
		return false;
	}	
	
	
	public boolean usuarioPodeAtenderTipoCirurgico(boolean isTipoAtendimentoCirurgico){
		
		if (!isTipoAtendimentoCirurgico){
			return true;
		}else { //é Tipo Atendimento Cirúrgico
			if (!isUsuarioVeterinario()){
				return true;
			} else { //usuário é Veterinário
			    if (isUsuarioVeterinarioCirurgiao()){
					return true;
				} else {
				    return false;			
				}				
			}

		}
	}
	
	public boolean usuarioPodeAtender2(TipoAtendimentoPaciente tipoAtendimento){
		
		if (!isUsuarioVeterinario()){ //não é Veterinário, tem acesso a tudo 
			return true;
		} 
		else if (tipoAtendimento == TipoAtendimentoPaciente.CIRURGICO ||
				tipoAtendimento == TipoAtendimentoPaciente.EXAME){
			if (isUsuarioVeterinarioCirurgiao() || isUsuarioVeterinarioLaudista()){
				return true;
			} else { //É veterinário Clínico
				return false;
			}
					
		} else { //Tipo de Atendimento Clínico ou Exame
			return true;
		}
			
	}

	public boolean usuarioPodeAtender(TipoAtendimentoPaciente tipoAtendimento){
		
		if (!isUsuarioVeterinario()){ //não é Veterinário, tem acesso a tudo 
			return true;
		} 
		else{
			if ((tipoAtendimento == TipoAtendimentoPaciente.CLINICO || tipoAtendimento == TipoAtendimentoPaciente.RETORNO)
				 && isUsuarioVeterinarioClinico()){
				return true;
			} else if ((tipoAtendimento == TipoAtendimentoPaciente.CIRURGICO ||
					    tipoAtendimento == TipoAtendimentoPaciente.AVALIACAO_CIRURGICA ||
					    tipoAtendimento == TipoAtendimentoPaciente.RETORNO_CIRURGICO) && isUsuarioVeterinarioCirurgiao() ){
			  return true;
			} else if (tipoAtendimento == TipoAtendimentoPaciente.EXAME && isUsuarioVeterinarioLaudista()){
				return true;
			} else {
				return false;
			}
		}
			
	}
	
	
	public boolean usuarioNaoPodeAtender(TipoAtendimentoPaciente tipoAtendimento){
		return !usuarioPodeAtender(tipoAtendimento);
	}

}
