package br.uece.favet.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Especie;
import br.uece.favet.repository.EspecieRepository;
import br.uece.favet.repository.filter.EspecieFilter;

@Named
@ViewScoped
public class PesquisaEspecieBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EspecieRepository especieRepository;

	private EspecieFilter filtro;

	private Set<Especie> especiesFiltradas;

	public PesquisaEspecieBean() {
		filtro = new EspecieFilter();
		especiesFiltradas = new HashSet<Especie>();
	}

	public void pesquisar() {
		especiesFiltradas = especieRepository.filtrados(filtro);
	}

	public Set<Especie> getEspeciesFiltradas() {
		return especiesFiltradas;
	}

	public EspecieFilter getFiltro() {
		return filtro;
	}

}
