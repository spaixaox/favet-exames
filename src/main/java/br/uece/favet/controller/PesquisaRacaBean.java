package br.uece.favet.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Raca;
import br.uece.favet.repository.RacaRepository;
import br.uece.favet.repository.filter.RacaFilter;

@Named
@ViewScoped
public class PesquisaRacaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private RacaRepository racaRepository;

	private RacaFilter filtro;

	private Set<Raca> racasFiltradas;

	public PesquisaRacaBean() {
		filtro = new RacaFilter();
		racasFiltradas = new HashSet<Raca>();
	}

	public void pesquisar() {
		racasFiltradas = racaRepository.filtrados(filtro);
	}

	public Set<Raca> getRacasFiltradas() {
		return racasFiltradas;
	}

	public RacaFilter getFiltro() {
		return filtro;
	}

}
