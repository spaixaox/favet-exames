package br.uece.favet.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Named;

import br.uece.favet.model.Especie;
import br.uece.favet.model.Raca;
import br.uece.favet.model.SolicitacaoExame;
import br.uece.favet.model.SolicitacaoExameItem;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroExameBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private SolicitacaoExame solicitacaoExame;
	private SolicitacaoExameItem solicitacaoExameItemEdicao;
	
	private SolicitacaoExameItem solicitacaoExameItemSelecionado;
	
	//@Inject
	//private CadastroSolicitacaoExameService cadastroSolicitacaoExameService;
	
	private List<Especie> especies;
	private List<Raca> racas;	
	
	public SolicitacaoExameItem getSolicitacaoExameItemEdicao() {
		return solicitacaoExameItemEdicao;
	}

	public void setSolicitacaoExameItemEdicao(SolicitacaoExameItem solicitacaoExameItemEdicao) {
		this.solicitacaoExameItemEdicao = solicitacaoExameItemEdicao;
	}
	
	public SolicitacaoExameItem getSolicitacaoExameItemSelecionado() {
		return solicitacaoExameItemSelecionado;
	}

	public void setSolicitacaoExameItemSelecionado(SolicitacaoExameItem solicitacaoExameItemSelecionado) {
		this.solicitacaoExameItemSelecionado = solicitacaoExameItemSelecionado;
	}

	public SolicitacaoExame getSolicitacaoExame() {
		return solicitacaoExame;
	}

	public void setSolicitacaoExame(SolicitacaoExame solicitacaoExame) {
		this.solicitacaoExame = solicitacaoExame;
	}

	public CadastroExameBean() {
		limpar();
	}
	
	public void limpar() {
		solicitacaoExame = new SolicitacaoExame();
		this.solicitacaoExameItemEdicao = new SolicitacaoExameItem();
	}

	public void editarSolicitacaoExameItem(SolicitacaoExameItem solicitacaoExameItem) {
		this.solicitacaoExameItemEdicao = solicitacaoExameItem;
	}	

	
	public void visualizarSolicitacaoExameItem(SolicitacaoExameItem solicitacaoExameItem) {
		this.solicitacaoExameItemEdicao = solicitacaoExameItem;
	}	
	
	public void novoSolicitacaoExameItem() {
		this.solicitacaoExameItemEdicao = new SolicitacaoExameItem();
	}
	
	public void excluirSolicitacaoExameItem() {
		
		try{
	//		String nome = solicitacaoExameItemSelecionado.getNome();
	//		int indice = this.solicitacaoExame.getAnimais().indexOf(solicitacaoExameItemSelecionado);
	//		this.solicitacaoExame.getAnimais().remove(indice);
       	    salvar();
       	    
			//FacesUtil.addInfoMessage("SolicitacaoExameItem "+nome+" excluído.");			
		} catch (NegocioException ne) {
			//FacesUtil.addErrorMessage("Mensagem21: "+ne.getMessage());
		} catch (Exception e) {
			
			//System.out.println("Erro getCause: "+e.getCause().toString());
			FacesUtil.addErrorMessage("Mensagem41: "+e.getMessage().trim());
		} 

	}	
	
	public void alterarSolicitacaoExameItem(){
		//		if (this.solicitacaoExameItemEdicao.isSolicitacaoExameItemValido()) {		
		//		salvar();
		//  	FacesUtil.addInfoMessage("SolicitacaoExameItem alterado!");
		//	}
	}
	
	public void adicionarSolicitacaoExameItem(){
		if (this.solicitacaoExameItemEdicao != null) {

			/*
			if (this.solicitacaoExameItemEdicao.isSolicitacaoExameItemValido()) {
				
	             this.solicitacaoExameItemEdicao.setSolicitacaoExame(this.solicitacaoExame);
	          	 this.solicitacaoExame.getAnimais().add(this.solicitacaoExameItemEdicao);
	          	 salvar();

	 			FacesUtil.addInfoMessage("SolicitacaoExameItem adicionado!");	             
			} */
		} else {
			FacesUtil.addInfoMessage("Dados do SolicitacaoExameItem não informados!");
		}
		

	}
	
	public void carregaSolicitacaoExame(){

	}
	
	public void salvar() {

		try {
		
	    	//this.solicitacaoExame = this.cadastroSolicitacaoExameService
			//			.salvarSemAnimais(this.solicitacaoExame);
	    	
    	
				FacesUtil.addInfoMessage("Proprietário e Animais salvos com sucesso!");

		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage("Mensagem2: "+ne.getMessage());
		} catch (Exception e) {
			
			if (e.getCause().toString().contains("ConstraintViolationException")) {
				FacesUtil.addErrorMessage("SolicitacaoExameItem não pode ser excluído, pois existem atendimentos para ele");	
			} else {
				FacesUtil.addErrorMessage("SolicitacaoExameItem não pode ser salvo.: "+e.getCause().toString());				
			}
			
			System.out.println("ErroSalvar getCause: "+e.getCause().toString());
			System.out.println("Erro: "+e.getMessage());
			
		} 
		
	}
	
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			/*
			this.especies = this.especieRepository.especies();
			*/
		}
	}
	
	public void carregarRacasPorEspecie(){
	//		if (this.solicitacaoExameItemEdicao.getEspecie().getId() != null){
   //			this.racas = this.racaRepository.racasPorEspecie(this.solicitacaoExameItemEdicao.getEspecie().getId());
     //	}
	}

	public boolean isEditando() {
		return this.solicitacaoExame.getId() != null;
	}

	public boolean isCadastrandoSolicitacaoExameItem() {
		return this.solicitacaoExameItemEdicao != null;
	}

	
}
