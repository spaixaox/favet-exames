package br.uece.favet.controller;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Veterinario;
import br.uece.favet.repository.VeterinarioRepository;
import br.uece.favet.repository.filter.VeterinarioFilter;

@Named
@ViewScoped
public class PesquisaVeterinarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private VeterinarioRepository veterinarioRepository;

	private VeterinarioFilter filtro;

	private Set<Veterinario> veterinariosFiltrados;

	public PesquisaVeterinarioBean() {
		filtro = new VeterinarioFilter();
		veterinariosFiltrados = new HashSet<Veterinario>();
	}

	public void pesquisar() {
		veterinariosFiltrados = veterinarioRepository.filtrados(filtro);
	}

	public Set<Veterinario> getVeterinariosFiltrados() {
		return veterinariosFiltrados;
	}

	public VeterinarioFilter getFiltro() {
		return filtro;
	}

}
