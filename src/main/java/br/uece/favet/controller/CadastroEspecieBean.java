package br.uece.favet.controller;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Especie;
import br.uece.favet.service.CadastroEspecieService;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroEspecieBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Especie especie;
	
	@Inject
	private CadastroEspecieService cadastroEspecieService;
	
	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public CadastroEspecieBean() {
        limpar();
	}

	public void limpar() {
		especie = new Especie();
	}

	public void salvar() {

		try {

	    	this.especie = this.cadastroEspecieService
							.salvar(this.especie);
		    	
			FacesUtil.addInfoMessage("Espécie salva com sucesso!");

		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage("Erro ao Salvar Espécie 1: "+ne.getMessage());
		} 	catch (Exception e){
			FacesUtil.addErrorMessage("Erro ao Salvar Espécie 2: "+e.getMessage()+", Causa :"+e.getCause());
		} 
		
	}
	
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			
		}
	}
	
	public boolean isEditando() {
		return this.especie.getId() != null;
	}
	
}
