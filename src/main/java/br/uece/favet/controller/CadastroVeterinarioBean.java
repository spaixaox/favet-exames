package br.uece.favet.controller;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Veterinario;
import br.uece.favet.service.CadastroVeterinarioService;
import br.uece.favet.service.NegocioException;
import br.uece.favet.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroVeterinarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Veterinario veterinario;
	
	@Inject
	private CadastroVeterinarioService cadastroVeterinarioService;
	
	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public CadastroVeterinarioBean() {
        limpar();
	}

	public void limpar() {
		veterinario = new Veterinario();
	}

	public void salvar() {

		try {

	    	this.veterinario = this.cadastroVeterinarioService
							.salvar(this.veterinario);
		    	
			FacesUtil.addInfoMessage("Veterinário salvo com sucesso!");

		} catch (NegocioException ne) {
			FacesUtil.addErrorMessage("Erro ao Salvar Veterinário 1: "+ne.getMessage());
		} 	catch (Exception e){
			FacesUtil.addErrorMessage("Erro ao Salvar Veterinário 2: "+e.getMessage()+", Causa :"+e.getCause());
		} 
		
	}
	
	public void inicializar() {
		if (FacesUtil.isNotPostback()) {
			
		}
	}
	
	public boolean isEditando() {
		return this.veterinario.getId() != null;
	}
	
}
