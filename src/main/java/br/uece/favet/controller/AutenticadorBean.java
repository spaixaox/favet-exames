package br.uece.favet.controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.uece.favet.model.Consultorio;
import br.uece.favet.model.Usuario;
import br.uece.favet.model.enums.TipoGrupoUsuario;
import br.uece.favet.repository.UsuarioRepository;
import br.uece.favet.util.jsf.FacesUtil;
import br.uece.favet.util.jsf.SessionUtil;

@Named
@SessionScoped
public class AutenticadorBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioRepository usuarioRepository;

	private String email;
	private String senha;
	private TipoGrupoUsuario tipoGrupoUsuario;
	private Consultorio consultorio;

	public String autentica() {

		Usuario usu = usuarioRepository.porEmailSenha(this.email, this.senha);
		
		if (usu != null) {
			SessionUtil.setParam("USUARIOLogado", usu);
			this.tipoGrupoUsuario = usu.getGrupo().getTipoGrupoUsuario();
			
			return "/Home.xhtml?faces-redirect=true";
		} else {
			SessionUtil.remove("USUARIOLogado");
			FacesUtil.addErrorMessage("Usuário ou senha informado inválido.");
			return null;
		}
	}

	public String registraSaida() {

		// REMOVER USUARIO DA SESSION
		SessionUtil.remove("USUARIOLogado");

		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		
		this.email = null;
		this.senha = null;
		this.tipoGrupoUsuario = null;
		this.consultorio = null;

		return "/Login?faces-redirect=true";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoGrupoUsuario getTipoGrupoUsuario() {
		return tipoGrupoUsuario;
	}
	
	public boolean isAdministrador(){
		return tipoGrupoUsuario.equals(TipoGrupoUsuario.ADMINISTRADOR);
	}

	public String getNomeConsultorioDaSessao() {
		String nome = null;
		if (this.consultorio != null){
			nome = "[no "+consultorio.getNome()+"]"; 
		}
		return nome;
	}
	
	public Consultorio getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(Consultorio consultorio) {
		this.consultorio = consultorio;
	}
	
}