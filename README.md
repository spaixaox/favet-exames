Partindo de um sistema já existente (Atendimento Veterinário), este projeto tem a proposta de desenvolver uma integração com os serviços de Laboratório de Patologia Clínica e Patologia Veterinária de um Hospital Veterinário.

Suas principais funcionalidades serão:
- Solicitação de Exames a partir do Atendimento Veterinário;
- Anexo de exames (.pdf) pelo Laboratório;
- Visualização dos Exames a partir do Atendimento Veterinário (exames do animal em atendimento);

Entidades Específicas para estas funcionalidades:
- Laboratorio
- CategoriaExame
- TipoExame
- SolicitacaoExame (Corpo dos dados da solicitação de exames)
- SolicitacaoExameItem (Poderá conter um ou mais tipos de exames)

Este projeto permanecerá com as mesmas tecnologias do sistema de Atendimento: JavaEE, JSF 2.2, Primefaces 6.0, JPA 2.0 com Hibernate, CDI (IoC/DI), JasperSoft (relatórios .jasper), C3P0, PostgreSql 9.3.