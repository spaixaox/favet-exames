
--Laboratórios
insert into laboratorio (id, nome, ativo) values (1,'Laboratório Patologia Clínica - UECE', true);
insert into laboratorio (id, nome, ativo) values (2,'Laboratório Patologia Veterinária - UECE', true);

--Categorias de Exames
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (1,'Perfis',1,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (2,'Hematologia',1,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (3,'Bioquímica Clínica',1,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (4,'Citologia',1,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (5,'Urinálise',1,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (6,'Outros',1,true);

insert into categoria_exame (id, nome, laboratorio_id, ativo) values (7,'Citologico',2,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (8,'Histopatológico',2,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (9,'Necrópsia',2,true);
insert into categoria_exame (id, nome, laboratorio_id, ativo) values (10,'Outros',2,true);

--Tipos Exames - Perfis - Lab. Patologia Clínica
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (1,1, 'Clínico 1 (HC, CR, TGP)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (2,1, 'Clínico 2 (HC, UR, CR, TGP, FA)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (3,1, 'Investigativo (HC, ALB, FA, CR, GLIC)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (4,1, 'Idoso (HC, UR, CR, TGP, TGO, ALB, GLIC)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (5,1, 'Ortopédico (UR, CR, CA, FOSF)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (6,1, 'Hepático (FA, TGP, TGO, GGT, ALBC)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (7,1, 'Geral (HC, CR, TGP, GLIC)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (8,1, 'Neurológico (HC, TGP, CK, CA, CR, GLIC)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (9,1, 'Grandes Animais (TGO, CA, COL, FA, TG, GLIC)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (10,1, 'Cavalos (HC, VHS, TGP, CR, CK)', true);
 --Tipos Exames - Hematologia - Lab. Patologia Clínica
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (11,2, 'Eritrograma', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (12,2, 'Leucograma', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (13,2, 'Contagem de plaquetas', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (14,2, 'Contadem de reticulócitos', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (15,2, 'Hemograma completo para mamíferos', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (16,2, 'Hemograma completo para silvestres e exóticos', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (17,2, 'Hemograma completo para animais de laboratório', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (18,2, 'Pesquisa de hemoparasitas', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (19,2, 'Pesquisa de Babésia', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (20,2, 'Pesquisa de Anaplasma', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (21,2, 'Pesquisa de Erlichia', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (22,2, 'Pesquisa de Hemobartonella', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (23,2, 'Pesquisa de Microfilária', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (24,2, 'Pesquisa de Inclusão de Lentz', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (25,2, 'Velocidade de Hemossedimentação (VHS)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (26,2, 'Outros', true);
--Tipos Exames - Bioquímica Clínica  - Lab. Patologia Clínica
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (27,3, 'Ácido úrico', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (28,3, 'Albumina', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (29,3, 'Amilase', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (30,3, 'AST/TGO', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (31,3, 'ALT/TGP', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (32,3, 'Bilirrubina total e frações', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (33,3, 'Cálcio', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (34,3, 'Colesterol total', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (35,3, 'Colesterol HDL', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (36,3, 'Colesterol LDL', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (37,3, 'Creatinina', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (38,3, 'CPK', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (39,3, 'Fibrinogênio', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (40,3, 'Fosfatase', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (41,3, 'Fósforo', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (42,3, 'Glicose', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (43,3, 'GGT', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (44,3, 'LDH', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (45,3, 'Lipase', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (46,3, 'Potássio', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (47,3, 'Proteina bioquímica', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (48,3, 'Sódio', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (49,3, 'Triglicerídeos', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (50,3, 'Uréia', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (51,3, 'Centrifugação (até máx. 2frs', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (52,3, 'Curya glicêmica (5 dosagens de glicose por dia)', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (53,3, 'Outros', true);
--Tipos Exames - Citologia  - Lab. Patologia Clínica
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (54,4, 'Análise de Líquidos Cavitários', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (55,4, 'Citologia de mucosa', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (56,4, 'Outros', true);
--Tipos Exames - Urinálise  - Lab. Patologia Clínica
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (57,5, 'Densidade', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (58,5, 'Físico-químico', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (59,5, 'Sedimentoscopia', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (60,5, 'Bacterioscopia', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (61,5, 'Sumário', true);
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (62,5, 'Outros', true);
--Tipos Exames - Outros  - Lab. Patologia Clínica
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (63,6, 'Outros', true);

--Tipos Exames - Citológico  - Lab. Patologia Veterinária
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (64,7, 'Citológico', true);
--Tipos Exames - Histopatológico  - Lab. Patologia Veterinária
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (65,8, 'Histopatológico', true);
--Tipos Exames -   - Lab. Patologia Veterinária
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (66,9, 'Necrópsia', true);
--Tipos Exames - Outros - Lab. Patologia Veterinária
insert into tipo_exame (id, categoria_exame_id, nome, ativo) values (67,10, 'Outros', true);

